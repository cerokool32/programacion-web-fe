export interface RegisterDto {
  nombre: string;
  fechaNacimiento: string;
  correo: string;
  clave: string;
  pais: number;
  educacion: number;
  contextura: number;
  sexo: number;
  sexoInteres: number;
  contexturaInteres: number;
}
