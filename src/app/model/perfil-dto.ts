export interface PerfilDto {
  nombre: string;
  fechaNacimiento: string;
  correo: string;
  pais: string;
  educacion: string;
  contextura: string;
  sexo: string;
  sexoInteres: string;
  contexturaInteres: string;
}
