import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './static/login/login.component';
import {RegisterComponent} from './static/register/register.component';
import {NotFoundComponent} from './static/not-found/not-found.component';
import {LoadPhotoComponent} from './static/register/load-photo/load-photo.component';
import {PerfilComponent} from './static/perfil/perfil.component';
import {AddAficionComponent} from './static/perfil/add-aficion/add-aficion.component';
import {AddAficionFotoComponent} from './static/perfil/add-aficion/add-aficion-foto/add-aficion-foto.component';
import {AddLugarComponent} from './static/perfil/add-lugar/add-lugar.component';
import {AddLugarPhotoComponent} from './static/perfil/add-lugar/add-lugar-photo/add-lugar-photo.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'register/photo/:id', component: LoadPhotoComponent},
  {path: 'perfil', component: PerfilComponent},
  {path: 'aficion', component: AddAficionComponent},
  {path: 'aficion/photo/:id', component: AddAficionFotoComponent},
  {path: 'lugar', component: AddLugarComponent},
  {path: 'lugar/photo/:id', component: AddLugarPhotoComponent},
  {path: '**', component: NotFoundComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
