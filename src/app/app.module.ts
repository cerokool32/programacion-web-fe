import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './static/login/login.component';
import {RegisterComponent} from './static/register/register.component';
import {
  MAT_DATE_LOCALE,
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {NotFoundComponent} from './static/not-found/not-found.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AngularFileUploaderModule} from 'angular-file-uploader';
import {LoadPhotoComponent} from './static/register/load-photo/load-photo.component';
import {PerfilComponent} from './static/perfil/perfil.component';
import {AddAficionComponent} from './static/perfil/add-aficion/add-aficion.component';
import {AddAficionFotoComponent} from './static/perfil/add-aficion/add-aficion-foto/add-aficion-foto.component';
import {ShowPicturePopUpComponent} from './static/show-picture-pop-up/show-picture-pop-up.component';
import {AddLugarComponent} from './static/perfil/add-lugar/add-lugar.component';
import { AddLugarPhotoComponent } from './static/perfil/add-lugar/add-lugar-photo/add-lugar-photo.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    LoadPhotoComponent,
    PerfilComponent,
    AddAficionComponent,
    AddAficionFotoComponent,
    ShowPicturePopUpComponent,
    AddLugarComponent,
    AddLugarPhotoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularFileUploaderModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    MatCheckboxModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-CO'},
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  entryComponents: [
    ShowPicturePopUpComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
