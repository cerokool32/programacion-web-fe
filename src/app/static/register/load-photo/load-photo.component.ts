import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-load-photo',
  templateUrl: './load-photo.component.html',
  styleUrls: ['./load-photo.component.scss']
})
export class LoadPhotoComponent implements OnInit, OnDestroy {

  private sub: any;
  private id: number;

  public afuConfig;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params['id'];
      this.afuConfig = {
        multiple: false,
        formatsAllowed: '.jpg',
        maxSize: '5',
        uploadAPI: {
          url: 'http://localhost:8080/perfil/foto/' + this.id,
        },
        hideProgressBar: false,
        hideResetBtn: true,
        hideSelectBtn: false
      };
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  goBack(): void {
    this.router.navigate(['/']);
  }

}
