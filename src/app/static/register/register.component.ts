import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegisterDto} from '../../model/register-dto';
import {Router} from '@angular/router';
import {KeyValueDto} from '../../model/key-value-dto';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public afuConfig = {
    multiple: false,
    formatsAllowed: '.jpg',
    maxSize: '5',
    uploadAPI: {
      url: 'http://localhost:8080/perfil/foto',
    },
    theme: 'default',
    hideProgressBar: false,
    hideResetBtn: true,
    hideSelectBtn: false
  };

  public pais: KeyValueDto[];
  public sexo: KeyValueDto[];
  public educacion: KeyValueDto[];
  public contextura: KeyValueDto[];

  private dto: RegisterDto;
  public formGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private router: Router, private http: HttpClient) {
  }

  ngOnInit() {
    this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/pais').subscribe(
      value => this.pais = value,
      error1 => alert('no se pudo obtener la informacion de pais')
    );
    this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/sexo').subscribe(
      value => this.sexo = value,
      error1 => alert('no se pudo obtener la informacion de sex')
    );
    this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/contextura').subscribe(
      value => this.contextura = value,
      error1 => alert('no se pudo obtener la informacion de contextura')
    );
    this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/educacion').subscribe(
      value => this.educacion = value,
      error1 => alert('no se pudo obtener la informacion de educacion')
    );
    this.dto = {
      clave: null,
      correo: null,
      fechaNacimiento: null,
      nombre: null,
      pais: null,
      contextura: null,
      contexturaInteres: null,
      educacion: null,
      sexoInteres: null,
      sexo: null
    };
    this.formGroup = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.minLength(3), Validators.email]],
      fechaNacimiento: ['', [Validators.required, Validators.minLength(3)]],
      clave: ['', [Validators.required, Validators.minLength(3)]],
      pais: ['', [Validators.required]],
      contextura: ['', [Validators.required]],
      contexturaInteres: ['', [Validators.required]],
      educacion: ['', [Validators.required]],
      sexoInteres: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
    });
    this.formGroup.get('nombre').valueChanges.subscribe(
      value => this.dto.nombre = value
    );
    this.formGroup.get('correo').valueChanges.subscribe(
      value => this.dto.correo = value
    );
    this.formGroup.get('fechaNacimiento').valueChanges.subscribe(
      value => this.dto.fechaNacimiento = value
    );
    this.formGroup.get('clave').valueChanges.subscribe(
      value => this.dto.clave = value
    );
    this.formGroup.get('pais').valueChanges.subscribe(
      value => this.dto.pais = value
    );
    this.formGroup.get('contextura').valueChanges.subscribe(
      value => this.dto.contextura = value
    );
    this.formGroup.get('contexturaInteres').valueChanges.subscribe(
      value => this.dto.contexturaInteres = value
    );
    this.formGroup.get('educacion').valueChanges.subscribe(
      value => this.dto.educacion = value
    );
    this.formGroup.get('sexoInteres').valueChanges.subscribe(
      value => this.dto.sexoInteres = value
    );
    this.formGroup.get('sexo').valueChanges.subscribe(
      value => this.dto.sexo = value
    );
  }

  public registrar(): void {
    console.log(this.dto);
    this.http.post<number>('http://localhost:8080/perfil', this.dto).subscribe(
      value => this.router.navigate(['/register/photo', value]),
      error1 => alert('Error registrando perfil')
    );
  }

  public cancelar(): void {
    this.router.navigate(['/']);
  }
}
