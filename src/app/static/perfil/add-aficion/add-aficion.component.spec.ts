import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAficionComponent } from './add-aficion.component';

describe('AddAficionComponent', () => {
  let component: AddAficionComponent;
  let fixture: ComponentFixture<AddAficionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAficionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAficionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
