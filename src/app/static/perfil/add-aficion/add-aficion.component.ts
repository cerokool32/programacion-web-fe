import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {KeyValueDto} from '../../../model/key-value-dto';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-aficion',
  templateUrl: './add-aficion.component.html',
  styleUrls: ['./add-aficion.component.scss']
})
export class AddAficionComponent implements OnInit {

  public aficiones: KeyValueDto[];
  public aficionesFormGroup: FormGroup;

  private aficion: number;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.aficionesFormGroup = this._formBuilder.group({
      aficion: ['', [Validators.required]],
    });

    this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/aficion').subscribe(
      value => this.aficiones = value,
      error1 => alert('No se pudo obtener lista de aficiones')
    );
    this.aficionesFormGroup.get('aficion').valueChanges.subscribe(
      value => this.aficion = value
    );
  }

  public addAficion(): void {
    const id = localStorage.getItem('id');
    this.http.post<any>('http://localhost:8080/perfil/' + id + '/aficion/' + this.aficion, {}).subscribe(
      value => this.router.navigate(['aficion/photo', this.aficion]),
      error1 => alert('no se pudo inscribir la aficion')
    );
  }

}
