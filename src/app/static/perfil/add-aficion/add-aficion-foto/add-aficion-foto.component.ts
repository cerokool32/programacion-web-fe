import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-aficion-foto',
  templateUrl: './add-aficion-foto.component.html',
  styleUrls: ['./add-aficion-foto.component.scss']
})
export class AddAficionFotoComponent implements OnInit, OnDestroy {

  private sub: any;
  private id: number;
  private perfilId: string;

  public afuConfig;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.perfilId = localStorage.getItem('id');
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params['id'];
      this.afuConfig = {
        multiple: false,
        formatsAllowed: '.jpg',
        maxSize: '5',
        uploadAPI: {
          url: 'http://localhost:8080/perfil/' + this.perfilId + '/aficion/' + this.id + '/photo',
        },
        hideProgressBar: false,
        hideResetBtn: true,
        hideSelectBtn: false
      };
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


  goBack(): void {
    this.router.navigate(['/perfil']);
  }

}
