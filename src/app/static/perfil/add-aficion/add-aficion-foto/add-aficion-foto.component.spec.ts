import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAficionFotoComponent } from './add-aficion-foto.component';

describe('AddAficionFotoComponent', () => {
  let component: AddAficionFotoComponent;
  let fixture: ComponentFixture<AddAficionFotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAficionFotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAficionFotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
