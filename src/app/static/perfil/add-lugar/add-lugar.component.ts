import {Component, OnInit} from '@angular/core';
import {KeyValueDto} from '../../../model/key-value-dto';
import {HttpClient} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-lugar',
  templateUrl: './add-lugar.component.html',
  styleUrls: ['./add-lugar.component.scss']
})
export class AddLugarComponent implements OnInit {

  private id: string;

  public paises: KeyValueDto[];
  public lugares: KeyValueDto[];

  public conocido = true;
  private lugarId: string;

  public formGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.id = localStorage.getItem('id');
    this.formGroup = this._formBuilder.group({
      pais: ['', [Validators.required]],
      lugar: ['', [Validators.required]],
      conocido: ['', [Validators.required]],
    });

    this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/pais').subscribe(
      value => this.paises = value,
      error1 => alert('no se pudo obtener la informacion de paises')
    );

    this.formGroup.get('pais').valueChanges.subscribe(
      value => this.http.get<Array<KeyValueDto>>('http://localhost:8080/datasource/pais/' + value + '/lugar').subscribe(
        v => this.lugares = v,
        error1 => alert('no se pudo obtener la informacion de lugares')
      )
    );

    this.formGroup.get('lugar').valueChanges.subscribe(
      value => this.lugarId = value
    );

    this.formGroup.get('conocido').valueChanges.subscribe(
      value => {
        if (value === 1) {
          this.conocido = true;
        } else {
          this.conocido = false;
        }
      }
    );
  }

  public addLugar(): void {
    if (this.conocido) {
      this.http.post<any>('http://localhost:8080/perfil/' + this.id + '/lugar/' + this.lugarId + '/conocido', '').subscribe(
        value => this.router.navigate(['lugar/photo', this.lugarId]),
        error1 => alert('no se pudo ingresar el lugar')
      );
    } else {
      this.http.post<any>('http://localhost:8080/perfil/' + this.id + '/lugar/' + this.lugarId, '').subscribe(
        value => this.router.navigate(['lugar/photo', this.lugarId]),
        error1 => alert('no se pudo ingresar el lugar')
      );
    }
  }

}
