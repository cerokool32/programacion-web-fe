import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-lugar-photo',
  templateUrl: './add-lugar-photo.component.html',
  styleUrls: ['./add-lugar-photo.component.scss']
})
export class AddLugarPhotoComponent implements OnInit, OnDestroy {

  private sub: any;
  private id: number;
  private perfilId: string;

  public afuConfig;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.perfilId = localStorage.getItem('id');
    this.sub = this.activatedRoute.params.subscribe(params => {
      this.id = +params['id'];
      this.afuConfig = {
        multiple: false,
        formatsAllowed: '.jpg',
        maxSize: '5',
        uploadAPI: {
          url: 'http://localhost:8080/perfil/' + this.perfilId + '/lugar/' + this.id + '/photo',
        },
        hideProgressBar: false,
        hideResetBtn: true,
        hideSelectBtn: false
      };
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }


  goBack(): void {
    this.router.navigate(['/perfil']);
  }

}
