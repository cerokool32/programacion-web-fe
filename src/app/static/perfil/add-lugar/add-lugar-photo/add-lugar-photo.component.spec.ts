import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLugarPhotoComponent } from './add-lugar-photo.component';

describe('AddLugarPhotoComponent', () => {
  let component: AddLugarPhotoComponent;
  let fixture: ComponentFixture<AddLugarPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLugarPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLugarPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
