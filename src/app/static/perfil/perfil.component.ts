import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {KeyValueDto} from '../../model/key-value-dto';
import {HttpClient} from '@angular/common/http';
import {MatDialog} from '@angular/material';
import {ShowPicturePopUpComponent} from '../show-picture-pop-up/show-picture-pop-up.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PerfilDto} from '../../model/perfil-dto';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

  private id: string;

  aficionesDisplayedColumns: string[] = ['Aficion', 'acciones'];
  aficionesDatasource;

  lugaresDisplayedColumns: string[] = ['Lugar', 'acciones'];
  lugaresDatasource;

  public formGroup: FormGroup;

  public perfiles: KeyValueDto[];

  public perfil: PerfilDto;
  public miPerfil: PerfilDto;

  constructor(private router: Router, private http: HttpClient, public dialog: MatDialog, private _formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.perfil = {
      contexturaInteres: null,
      sexoInteres: null,
      sexo: null,
      contextura: null,
      educacion: null,
      pais: null,
      correo: null,
      fechaNacimiento: null,
      nombre: null,
    };
    this.miPerfil = {
      contexturaInteres: null,
      sexoInteres: null,
      sexo: null,
      contextura: null,
      educacion: null,
      pais: null,
      correo: null,
      fechaNacimiento: null,
      nombre: null,
    };

    this.id = localStorage.getItem('id');
    this.http.get<Array<KeyValueDto>>('http://localhost:8080/perfil/' + this.id + '/aficion').subscribe(
      value => this.aficionesDatasource = value,
      error1 => alert('No se pudieron obtener las aficiones')
    );

    this.http.get<Array<KeyValueDto>>('http://localhost:8080/perfil/' + this.id + '/lugar').subscribe(
      value => this.lugaresDatasource = value,
      error1 => alert('No se pudieron obtener las aficiones')
    );

    this.http.get<Array<KeyValueDto>>('http://localhost:8080/perfil/perfiles').subscribe(
      value => this.perfiles = value,
      error1 => alert('no se pudieron obtener los perfiles')
    );

    this.formGroup = this._formBuilder.group({
      perfil: ['', [Validators.required]],
    });

    this.formGroup.get('perfil').valueChanges.subscribe(
      value => this.http.get<PerfilDto>('http://localhost:8080/perfil/perfiles/' + value).subscribe(
        value1 => this.perfil = value1,
        error1 => alert('no se pudo obtener informacion del perfil')
      )
    );
    this.http.get<PerfilDto>('http://localhost:8080/perfil/perfiles/' + this.id).subscribe(
      value => this.miPerfil = value,
      error1 => alert('No se pudo obtener su informacion de perfil')
    );
  }

  public addAficion(): void {
    this.router.navigate(['aficion']);
  }

  public addLugar(): void {
    this.router.navigate(['lugar']);
  }

  public verFotoAficion(aficionId: number): void {
    const dialogRef = this.dialog.open(ShowPicturePopUpComponent, {
      width: '50%',
      data: {url: 'http://localhost:8080/perfil/' + this.id + '/aficion/' + aficionId + '/photo'}
    });
  }

  public verFotoLugar(lugarId: number): void {
    const dialogRef = this.dialog.open(ShowPicturePopUpComponent, {
      width: '50%',
      data: {url: 'http://localhost:8080/perfil/' + this.id + '/lugar/' + lugarId + '/photo'}
    });
  }
}
