import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginDto} from '../../model/login-dto';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private dto: LoginDto;
  public formGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder, private router: Router, private http: HttpClient) {
  }

  ngOnInit() {
    this.dto = {
      correo: null,
      clave: null
    };
    this.formGroup = this._formBuilder.group({
      login: ['', [Validators.required, Validators.minLength(3), Validators.email]],
      password: ['', [Validators.required, Validators.min(3)]]
    });
    this.formGroup.get('login').valueChanges.subscribe(
      value => this.dto.correo = value
    );
    this.formGroup.get('password').valueChanges.subscribe(
      value => this.dto.clave = value
    );
  }

  public login(): void {
    this.http.post<number>('http://localhost:8080/perfil/login', this.dto).subscribe(
      value => {
        if (value != null) {
          localStorage.setItem('id', '' + value);
          this.router.navigate(['/perfil']);
        } else {
          alert('Usuario/Contraseña errados');
        }
      },
      error1 => alert('Error de comunicación')
    );
    console.log(this.dto);
  }

  public register(): void {
    this.router.navigate(['/register']);
  }

}
