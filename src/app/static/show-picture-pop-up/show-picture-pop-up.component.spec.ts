import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPicturePopUpComponent } from './show-picture-pop-up.component';

describe('ShowPicturePopUpComponent', () => {
  let component: ShowPicturePopUpComponent;
  let fixture: ComponentFixture<ShowPicturePopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPicturePopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPicturePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
