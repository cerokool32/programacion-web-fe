import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-show-picture-pop-up',
  templateUrl: './show-picture-pop-up.component.html',
  styleUrls: ['./show-picture-pop-up.component.scss']
})
export class ShowPicturePopUpComponent implements OnInit {

  public src: string;

  constructor(public dialogRef: MatDialogRef<ShowPicturePopUpComponent>,
              @Inject(MAT_DIALOG_DATA) public data: MAT_DIALOG_DATA) {
  }

  ngOnInit() {
    this.src = this.data.url;
  }

}
